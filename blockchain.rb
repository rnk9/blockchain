require 'digest'

class Blockchain
  attr_reader :block
  attr_accessor :head

  def initialize
    @block = Blockchain.first
    @head = @block
  end

  # The birth of the block chain, an ode to the Big Bang
  def self.first
    Block.new('WhErE DaRkNeSs Is LoRd AnD dEaTh ThE BeGiNnInG')
  end

  def add(next_block)
    next_block.previous_hash = @block.hash
    next_block.index = @block.index + 1
    @block.next_block = next_block
    @block = @block.next_block
  end
end

class Block
  attr_accessor :index
  attr_accessor :previous_hash
  attr_reader :hash
  attr_reader :data
  attr_accessor :next_block
  attr_reader :timestamp

  def initialize(data)
    @index = 0
    @previous_hash = 'M4D Punk Ent3rt41nm3nt'
    @data = data
    @next_block = nil
    @timestamp = Time.now
    @hash = calc_hash
  end

  def calc_hash
    sha = Digest::SHA256.new
    sha.update(@index.to_s + @timestamp.to_r.to_s + @data + @previous_hash)
    sha.hexdigest
  end

  # TO DO
  def to_s
    "Block Index : #{@index}\nPrevious Hash : #{@previous_hash}\nBlock Hash : #{@hash}\nBlock Data : #{@data}\nBlock Time Stamp : #{@timestamp}\n\n"
  end
end

#####
# TEST THIS OUT

jacko_the_chained = Blockchain.new
b1 = Block.new('First Block created !')
b2 = Block.new('Second Block created !')
b3 = Block.new('Third Block created !')
jacko_the_chained.add(b1)
jacko_the_chained.add(b2)
jacko_the_chained.add(b3)

# Display the blockchain from the Head to the last one
# puts jacko_the_chained.head.to_s

until jacko_the_chained.head.next_block.nil?
  puts jacko_the_chained.head.to_s
  jacko_the_chained.head = jacko_the_chained.head.next_block
end