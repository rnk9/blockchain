# TO DO
# Python BLockchain Doc : https://github.com/llSourcell/Simple_Blockchain_in_5_Minutes/blob/master/blockchain%20(1).ipynb

require 'digest'

class Blockchain
  attr_reader :block
  attr_accessor :head
  
  def initialize
    @mining_difficulty = 20
    # Max number storable in 32bits number
    @max_nounce = 2**32
    # Target hash for mining
    @target_hash = 2**(256 - @mining_difficulty)
    @block = Blockchain.first
    @head = @block
  end

  # The birth of the block chain, an ode to the Big Bang
  def self.first
    Block.new('WhErE DaRkNeSs Is LoRd AnD dEaTh ThE BeGiNnInG')
  end

  def add(next_block)
    next_block.previous_hash = @block.hash
    next_block.index = @block.index + 1
    @block.next_block = next_block
    @block = @block.next_block
  end

  # Determines whether or not we can add a block to the blockchain
  def mine(next_block)
    # from 0 to 2^32
    for n in 0..@max_nounce
      # is the value of the given block's hash less than our target value?
      if @block.calc_hash_with_nonce.to_i <= @target_hash
        add(next_block)
        break
      else
        next_block.nonce += 1
      end
    end
  end
end

class Block
  attr_accessor :index
  attr_accessor :previous_hash
  attr_reader :hash
  attr_reader :data
  attr_accessor :next_block
  attr_reader :timestamp
  attr_accessor :nonce

  def initialize(data)
    @index = 0
    @previous_hash = 'M4D Punk Ent3rt41nm3nt'
    @data = data
    @next_block = nil
    @timestamp = Time.now
    @nonce = 0
    @hash = calc_hash_with_nonce
  end

  def calc_hash_with_nonce
    sha = Digest::SHA256.new
    sha.update(@nonce.to_s + @index.to_s + @timestamp.to_r.to_s + @data + @previous_hash)
    sha.hexdigest
  end
  
  # TO DO
  def to_s
    "Block Index : #{@index}\nPrevious Hash : #{@previous_hash}\nBlock Hash : #{@hash}\nBlock Data : #{@data}\nBlock Time Stamp : #{@timestamp}\nBlock Nounce : #{@nonce}\n\n"
  end
end

#####
# TEST THIS OUT
jacko_the_chained = Blockchain.new
b1 = Block.new('First Block created !')
b2 = Block.new('Second Block created !')
b3 = Block.new('Third Block created !')
jacko_the_chained.mine(b1)
jacko_the_chained.mine(b2)
jacko_the_chained.mine(b3)

until jacko_the_chained.head.nil?
  puts jacko_the_chained.head.to_s
  jacko_the_chained.head = jacko_the_chained.head.next_block
end